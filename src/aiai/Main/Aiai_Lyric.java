package aiai.Main;

/**童謡アイアイを表示する部品
 *以下、アイアイ歌詞
 * @author 200217AM
 */

public class Aiai_Lyric {

	/**歌詞「ア イ ア イ （ア イ ア イ）」を表示
	 * @return
	 */
	public static String sayAiai() {
		return "ア イ ア イ （ア イ ア イ） ";
	}

	/**同じメッセージを複数回表示
	 * @param msg
	 * @param count
	 * @return
	 */
	public static String repeatMessage(String msg,int count){
		String repeatMsg="";
		for(int i=0;i<count;i++){
			repeatMsg+=msg;
		}
		return repeatMsg;
	}

	/**改行する
	 * @return
	 */
	public static String newLine(){
		return "\n";
	}

	/**1番歌詞「お さ る さ ん だ よ」を表示
	 * @return
	 */
	public static String sayMonkey(){
		return "お さ る さ ん だ よ";
	}

	/**1番歌詞「み な み の し ま の」を表示
	 * @return
	 */
	public static String saySouth(){
		return "み な み の し ま の";

	}
	/**１番歌詞「し っ ぽ の な が い」を表示
	 * @return
	 */
	public static String sayLongTail(){
		return "し っ ぽ の な が い";
	}

	//以下2番歌詞

	/**2番歌詞「お さ る さ ん だ ね」を表示
	 * @return
	 */
	public static String sayMonkeySecond(){
		 return "お さ る さ ん だ ね";
	}

	/**2番歌詞「き の は の お う ち」を表示
	 * @return
	 */
	public static String homeTree(){
		return "き の は の お う ち";
	}

	/**2番歌詞「お め め の ま る い」を表示
	 * @return
	 */
	public static String roundEyes(){
		return "お め め の ま る い";
	}

	/**アイアイ、童謡歌詞実行部
	 *
	 */
	public static void execute(){
		String lyric="";

		//1番歌詞 1行目
		lyric+=repeatMessage(sayAiai(),2);
		lyric+=newLine();
		lyric+=sayMonkey();
		lyric+=newLine();

		//2行目
		lyric+=repeatMessage(sayAiai(), 2);
		lyric+=newLine();
		lyric+=saySouth();
		lyric+=newLine();

		//3行目
		lyric+=repeatMessage(sayAiai(), 4);
		lyric+=newLine();
		lyric+=sayLongTail();
		lyric+=newLine();

		//4行目
		lyric+=repeatMessage(sayAiai(), 2);
		lyric+=newLine();
		lyric+=sayMonkey();
		lyric+=newLine();
		lyric+=newLine();

		//2番歌詞 1行目
		lyric+=repeatMessage(sayAiai(), 2);
		lyric+=newLine();
		lyric+=sayMonkeySecond();
		lyric+=newLine();

		//2行目
		lyric+=repeatMessage(sayAiai(), 2);
		lyric+=newLine();
		lyric+=homeTree();
		lyric+=newLine();

		//3三行目
		lyric+=repeatMessage(sayAiai(), 4);
		lyric+=newLine();
		lyric+=roundEyes();
		lyric+=newLine();

		//4行目
		lyric+=repeatMessage(sayAiai(), 2);
		lyric+=newLine();
		lyric+=sayMonkeySecond();

			System.out.println(lyric);

		//以降、自作のメソッドを使ってアイアイを完成させてください。
	}
}
